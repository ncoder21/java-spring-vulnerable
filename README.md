# OWASP Benchmark

The OWASP Benchmark Project is a Java test suite designed to verify the
speed and accuracy of vulnerability detection tools. The initial version is
intended to support Static Analysis Security Testing Tools (SAST). A future
release will support Dynamic Analysis Security Testing Tools (DAST), like
<a href="https://www.owasp.org/index.php/ZAP">OWASP ZAP</a>, and Interactive
Analysis Security Testing Tools (IAST). The goal is that this test application
is fully runnable and all the vulnerabilities are actually exploitable so
its a fair test for any kind of application vulnerability detection tool.

The project documentation is all on the OWASP site at the <a
href="https://www.owasp.org/index.php/Benchmark">OWASP Benchmark</a> project
pages. Please refer to that site for all the project details.

The current latest release is v1.2. Note that all the releases that
are available here: https://github.com/OWASP/Benchmark/releases, are
historical. The latest release is always available live by simply cloning
or pulling the head of this repository (i.e., git pull).

## GitLab Notes

The file [./expectedresults.json](./expectedresults.json) is the result
of running the script at [./csv_to_json_converter.rb](./csv_to_json_converter.rb) on
the original CSV expected results file [./expectedresults-1.2.csv](./expectedresults-1.2.csv).

## Accessing Latest SAST `gl-sast-report.json`

[DOWNLOAD HERE](https://gitlab.com/gitlab-org/security-products/benchmark-suite/java-spring-owasp_benchmark/-/jobs/artifacts/master/raw/gl-sast-report.json?job=sast)

## Creating a Standalone `.war` File

Install `javac` and `mvn`:

```
sudo apt-get update && sudo apt-get install maven openjdk-8-jdk-headless openjdk-8-jre
```

Compile and package the benchmark:

```
mvn compile
mvn package
```

The final `.war` should exist at `target/benchmark.war`
